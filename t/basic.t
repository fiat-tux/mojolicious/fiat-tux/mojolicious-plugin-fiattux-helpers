# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -strict;

use Test::More;
use Mojolicious::Lite;
use Test::Mojo;

plugin 'FiatTux::Helpers';

get '/' => sub {
    my $c = shift;
    $c->render(text => $c->ip);
};

my $t = Test::Mojo->new;
$t->get_ok('/')
  ->status_is(200)
  ->content_like(qr/^127\.0\.0\.1 remote port: \d+$/);

done_testing();

# NAME

Mojolicious::Plugin::FiatTux::Helpers - Personal (unpublished) Mojolicious plugin that provides commonly used helpers for my softwares.

# SYNOPSIS

    # Mojolicious
    $self->plugin('FiatTux::Helpers');

    # Mojolicious::Lite
    plugin 'FiatTux::Helpers';

# DESCRIPTION

[Mojolicious::Plugin::FiatTux::Helpers](https://framagit.org/fiat-tux/mojolicious/fiat-tux/mojolicious-plugin-fiattux-Helpers) is a [Mojolicious](https://metacpan.org/pod/Mojolicious) plugin.

# METHODS

[Mojolicious::Plugin::FiatTux::Helpers](https://framagit.org/fiat-tux/mojolicious/fiat-tux/mojolicious-plugin-fiattux-helpers) inherits all methods from
[Mojolicious::Plugin](https://metacpan.org/pod/Mojolicious::Plugin) and implements the following new ones.

## register

    $plugin->register(Mojolicious->new);

Register plugin in [Mojolicious](https://metacpan.org/pod/Mojolicious) application.

## ip

    my $ip = $app->ip;

Return IP address and remote port of request, superseeded by X-Forwarded-For and X-Remote-Port if presents.

Format:

    <IP> remote port: <remote port>

## available\_langs

    my $langs = $app->available_langs;

Return the codes of the available langs in default and chosen theme

## iso639\_native\_name

    my $native_name = $app->iso639_native_name($iso639_code);

Return the native name of a language from its ISO639 code

## shortener

    my $short = $app->shortener($length);

Return a random string (\[a-zA-Z0-9\_\]) of $length characters

## swift

    my $swift = $app->swift;

Return a Net::OpenStack::Swift object, configured from `$app-`config('swift')>

## check\_swift\_container

    $app->check_swift_container();

Check if `$app-`config('swift')->{container}> exists. Tries to create it if it doesn’t exist.

## find\_swift\_container

    my ($headers, $containers) = $c->swift->get_account(url => $storage_url, token => $token);
    my $exists = $app->find_swift_container($containers);

Return a boolean indicating if `$app-`config('swift')->{container}> exists in the containers list returned by Net::OpenStack::Swift->get\_account();

# SEE ALSO

[Mojolicious](https://metacpan.org/pod/Mojolicious), [Mojolicious::Guides](https://metacpan.org/pod/Mojolicious::Guides), [https://mojolicious.org](https://mojolicious.org).

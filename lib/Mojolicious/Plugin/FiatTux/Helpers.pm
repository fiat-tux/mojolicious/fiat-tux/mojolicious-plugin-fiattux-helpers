# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package Mojolicious::Plugin::FiatTux::Helpers;
use Mojo::Base 'Mojolicious::Plugin';
use Mojo::Collection;
use Mojo::File;
use Mojo::Util qw(decode);
use Data::Entropy qw(entropy_source);
use ISO::639_1;

our $VERSION = '0.12';

sub register {
    my ($self, $app) = @_;

    $app->helper(ip                 => \&_ip);
    $app->helper(just_ip            => \&_just_ip);
    $app->helper(shortener          => \&_shortener);
    $app->helper(available_langs    => \&_available_langs);
    $app->helper(iso639_native_name => \&_iso639_native_name);

    if ($app->config('swift')) {
        require Net::OpenStack::Swift;
        $app->helper(swift                 => \&_swift);
        $app->helper(check_swift_container => \&_check_swift_container);
        $app->helper(find_swift_container  => \&_find_swift_container);
    }

    # Force language
    my $cookie_name = $app->moniker.'_lang';
    $app->hook(
        before_dispatch => sub {
            my $c = shift;
            my $l = $c->every_cookie($cookie_name)->[0];
            $c->languages($l) if $l;
        }
    );
}

sub _ip {
    my $c           = shift;
    my $ip          = $c->req->headers->header('X-Forwarded-For') || $c->tx->remote_address;
    my $remote_port = $c->req->headers->header('X-Remote-Port')   || $c->tx->remote_port;

    return sprintf('%s remote port: %s', $ip, $remote_port);
}

sub _just_ip {
    my $c     = shift;

    my $proxy = $c->req->headers->header('X-Forwarded-For');
    my $ip    = ($proxy) ? $proxy : $c->tx->remote_address;

    return $ip;
}

sub _available_langs {
    my $c = shift;

    state $langs = Mojo::Collection->new(
        glob($c->app->home->rel_file('themes/'.$c->config('theme').'/lib/'.ucfirst($c->app->moniker).'/I18N/*po')),
        glob($c->app->home->rel_file('themes/default/lib/'.ucfirst($c->app->moniker).'/I18N/*po'))
    )->map(
        sub {
            Mojo::File->new($_)->basename('.po');
        }
    )->uniq->sort(
        sub {
            $c->l($a) cmp $c->l($b)
        }
    )->to_array;
}

sub _iso639_native_name {
    my $c = shift;
    my $iso639_1 = get_iso639_1(shift);
    if ($iso639_1) {
        return ucfirst(decode 'UTF-8', $iso639_1->{nativeName});
    } else {
        return undef;
    }
}

sub _shortener {
    my $c      = shift;
    my $length = shift;

    my @chars  = ('a'..'h', 'j', 'k', 'm'..'z','A'..'H', 'J'..'N', 'P'..'Z','0'..'9', '_');
    my $result = '';
    foreach (1..$length) {
        $result .= $chars[entropy_source->get_int(scalar(@chars))];
    }
    return $result;
}

sub _swift {
    my $c = shift;

    state $swift = Net::OpenStack::Swift->new($c->app->config('swift'));
    return $swift;
}

sub _check_swift_container {
    my $c = shift;

    my ($storage_url, $token)  = $c->swift->get_auth();
    my ($headers, $containers) = $c->swift->get_account(url => $storage_url, token => $token);
    unless ($c->find_swift_container($containers)) {
        $c->swift->put_container(container_name => $c->app->config('swift')->{container});
        my ($headers, $containers) = $c->swift->get_account(url => $storage_url, token => $token);
        die sprintf("Swift container %s not found, and unable to create it.", $c->app->config('swift')->{container}) unless $c->find_swift_container($containers);
    }
}

sub _find_swift_container {
    my $c          = shift;
    my $containers = shift;

    my $found_container = 0;
    foreach my $container (@{$containers}) {
        $found_container = 1 if $container->{name} eq $c->app->config('swift')->{container};
    }

    return $found_container;
}

1;
__END__

=encoding utf8

=head1 NAME

Mojolicious::Plugin::FiatTux::Helpers - Personal (unpublished) Mojolicious plugin that provides commonly used helpers for my softwares.

=head1 SYNOPSIS

  # Mojolicious
  $self->plugin('FiatTux::Helpers');

  # Mojolicious::Lite
  plugin 'FiatTux::Helpers';

=head1 DESCRIPTION

L<Mojolicious::Plugin::FiatTux::Helpers|https://framagit.org/fiat-tux/mojolicious/fiat-tux/mojolicious-plugin-fiattux-Helpers> is a L<Mojolicious> plugin.

=head1 METHODS

L<Mojolicious::Plugin::FiatTux::Helpers|https://framagit.org/fiat-tux/mojolicious/fiat-tux/mojolicious-plugin-fiattux-helpers> inherits all methods from
L<Mojolicious::Plugin> and implements the following new ones.

=head2 register

  $plugin->register(Mojolicious->new);

Register plugin in L<Mojolicious> application.

=head2 ip

  my $ip = $app->ip;

Return IP address and remote port of request, superseeded by X-Forwarded-For and X-Remote-Port if presents.

Format:

  <IP> remote port: <remote port>

=head2 available_langs

  my $langs = $app->available_langs;

Return the codes of the available langs in default and chosen theme

=head2 iso639_native_name

  my $native_name = $app->iso639_native_name($iso639_code);

Return the native name of a language from its ISO639 code

=head2 shortener

  my $short = $app->shortener($length);

Return a random string ([a-zA-Z0-9_]) of $length characters

=head2 swift

  my $swift = $app->swift;

Return a Net::OpenStack::Swift object, configured from C<$app->config('swift')>

=head2 check_swift_container

  $app->check_swift_container();

Check if C<$app->config('swift')->{container}> exists. Tries to create it if it doesn’t exist.

=head2 find_swift_container

  my ($headers, $containers) = $c->swift->get_account(url => $storage_url, token => $token);
  my $exists = $app->find_swift_container($containers);

Return a boolean indicating if C<$app->config('swift')->{container}> exists in the containers list returned by Net::OpenStack::Swift->get_account();

=head1 SEE ALSO

L<Mojolicious>, L<Mojolicious::Guides>, L<https://mojolicious.org>.

=cut
